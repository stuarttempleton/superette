module ApplicationHelper
	def gravatar_url( address, opts = {} )
		s = opts[:size] == nil ? '50' : opts[:size]
		r = opts[:rating] == nil ? 'pg' : opts[:rating]
		d = opts[:default] == nil ? 'mm' : opts[:default]
		return "http://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(address)}?s=#{s}&r=#{r}&d=#{d}"
	end

	def gravatar_hash( address )
		return Digest::MD5.hexdigest(address)
	end

	include ActionView::Helpers::JavaScriptHelper

	def self.escape_js( text )
		@instance ||= self.new
		return @instance.escape_javascript( text )
	end

	def tracking_link( tracking_number )
		if tracking_number == "" || tracking_number == nil
			return "<td></td><td>No tracking number</td>".html_safe
		end

		upsPattern = /\b(1Z ?[0-9A-Z]{3} ?[0-9A-Z]{3} ?[0-9A-Z]{2} ?[0-9A-Z]{4} ?[0-9A-Z]{3} ?[0-9A-Z]|[\dT]\d\d\d ?\d\d\d\d ?\d\d\d)\b/i
		fedexPatterns = [ /(\b96\d{20}\b)|(\b\d{15}\b)|(\b\d{12}\b)/,
				  /\b((98\d\d\d\d\d?\d\d\d\d|98\d\d) ?\d\d\d\d ?\d\d\d\d( ?\d\d\d)?)\b/,
				  /^[0-9]{15}$/	]

		uspsPatterns = [ /(\b\d{30}\b)|(\b91\d+\b)|(\b\d{20}\b)/,
				 /^E\D{1}\d{9}\D{2}$|^9\d{15,21}$/,
				 /^91[0-9]+$/,
				 /^[A-Za-z]{2}[0-9]+US$/ ]

		if tracking_number =~ upsPattern
			return ("<td>" + image_tag( root_url + "images/ups_logo.png" ) + "</td><td><a href=\"http://wwwapps.ups.com/WebTracking/track?track=yes&amp;trackNums=#{tracking_number}\">#{tracking_number}</a></td>").html_safe
		end

		fedexPatterns.each do |fedexPattern|
			if tracking_number =~ fedexPattern
				return  ("<td>" + image_tag(  root_url + "images/fedex_logo.png" ) + "</td><td><a href=\"http://www.fedex.com/Tracking?action=track&amp;tracknumbers=#{tracking_number}\">#{tracking_number}</a></td>").html_safe
			end
		end

		uspsPatterns.each do |uspsPattern|
			if tracking_number =~ uspsPattern
				return ("<td>" + image_tag(  root_url + "images/usps_logo.png" ) + "</td><td><a href=\"http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum=#{tracking_number}\">#{tracking_number}</a></td>").html_safe
			end
		end
		return ( "<td></td><td>" + tracking_number + "</td>" ).html_safe
	end
end
