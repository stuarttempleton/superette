class LoginController < ApplicationController
	  
  def index
  end
  
  def reset
  	  shop = Shop.find_by_email(params[:id])
  	  shop.save if shop.initiatereset
  
  	  flash[:notice] = "Login information has been emailed to the shop owner."
  	  redirect_to :controller=>"landing", :action=>"index" and return
  end
  
  def verify
  	  session["authenticated"] = false
  	  shop = Shop.find_by_verification(params[:id])
  	  if shop.reset(params[:id])
		  shop.save 
		  flash[:notice] = "Password reset! You should change your password <i>NOW</i>!"
		  session["authenticated"] = true
		  redirect_to "/#{shop.slug}/shops/edit/#{shop.id}" and return
	  end
	  
  	  flash[:notice] = "Password reset verification has failed."
  	  redirect_to :controller=>"landing", :action=>"index" and return
  end
  
  def authenticate
  	  session["authenticated"] = false
  	  if Shop.authenticate(params[:email], params[:password])
  	  	  session["authenticated"] = true
  	  	  shop = Shop.find_by_email(params[:email])
  	  	  redirect_to "/#{shop.slug}/shops/edit/#{shop.id}" and return
  	  elsif ( (params[:name] != '') && (params[:name] != nil) )
  	  	  shop = Shop.new({:name=> params[:name], :email=>params[:email], :slug=>params[:name], :password=>params[:password]})
  	  	  shop.password = Shop.find_by_sql(["select password( ? ) as pass", params[:password]])[0].pass
  	  	  if shop.save
			  session["authenticated"] = true
			  redirect_to "/#{shop.slug}/shops/edit/#{shop.id}" and return
  	  	  end
  	  	  
  	  end
  	  flash[:notice] = "Login failed. <a href='/login/reset/?id=#{params[:email]}'>Forgot your password</a>?"
  	  redirect_to :controller=>"landing", :action=>"index" and return
  end
  
  def logout
  	  session["authenticated"] = false
  	  redirect_to :controller=>"landing" and return
  end
  
end
