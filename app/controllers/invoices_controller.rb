class InvoicesController < ApplicationController
	  before_filter :load_shop
	  def search
		    email = params[:email]
		    zipcode = params[:zipcode]

		    # Sanity checks
		    if email == "" || email == ""
			    render :text=> "Error: You need an email address and zipcode to search" and return
		    end

		    invoices = @shop.invoices.where( "invoices.zip = ? AND invoices.email = ?", zipcode, email ).order( "invoices.created_at DESC" )
		    if invoices.count == 0
			    render :text=> "Error: Order not found" and return
		    end

		    # list out each invoice
		    render :partial=>"invoice_list", :locals=>{ :invoices=> invoices } and return
	  end

	  def show
		    id = params[:invoice_id]
		    transaction_id = @shop.invoices.find( id ).transaction_id
		    paypal_request = Paypal::Express::Request.new( :username=> @shop.paypal_account.user,
								   :password=> @shop.paypal_account.password,
								   :signature=> @shop.paypal_account.signature )

		    details = paypal_request.transaction_details( transaction_id )
		    if details.ack.downcase != "success"
			render :text=> "Error: Order not found" and return
		    end

		    # fixup the descriptions  and subtotal because paypal loses them
		    subtotal = 0
		    details.items.each do |item|
			item.description = Stuff.find( item.number ).description
			subtotal += item.amount.total
		    end
		    details.amount.item = subtotal

		    # create a new transaction and render the invoice
		    transaction = Transaction.new( {:details=> details, :invoice_id=> id, :currency=>@shop.paypal_account.currency} )
		    render :partial=>"invoice", :locals=>{ :transaction=> transaction }
	  end

	  def load_shop
		    @shop = Shop.find_by_slug(params[:slug])
	  end
end
