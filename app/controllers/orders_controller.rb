class OrdersController < ApplicationController
	before_filter :check_auth
	before_filter lambda{ ActionMailer::Base.default_url_options = {:host => request.host_with_port} }
	# GET /stuffs
	# GET /stuffs.json
	def index
		# our list is really three lists. This allows us to do some quick and dirty organization
		@new = @shop.invoices.new_orders
		@processed = @shop.invoices.processed_orders
		@canceled = @shop.invoices.cancelled_orders
	end

	def show
		@transaction = load_invoice( params[:id] )
		@processing = false
	end

	def process_order
		@transaction = load_invoice( params[:id] )
		@processing = true
		order = @shop.invoices.next_new_order( params[:id] )[0]
		@next_id = order ? order.id : nil
	end

	def save
		fulfill_order( params[:id], params[:tracking] )
		redirect_to :action=>"index"
	end

	def next
		fulfill_order( params[:id], params[:tracking] )
		redirect_to( :action=>"process_order", :id=>params[:next_id] )
	end

	def cancel
		invoice = Invoice.find( params[:id] )
		transaction_id = invoice.transaction_id
		paypal_request = Paypal::Express::Request.new( :username=> @shop.paypal_account.user,
							       :password=> @shop.paypal_account.password,
							       :signature=> @shop.paypal_account.signature )
		response = paypal_request.refund!( transaction_id )
		if response.ack.downcase != "success"
			flash[:notice] = "Error: There was a problem canceling the order. You may need to cancel the order from inside your Paypal account."
		end
		invoice.canceled = true
		invoice.save
		redirect_to :action=>"index"
	end

	private

	def check_auth
		@edit = false
		@edit = true if session["authenticated"]
		@shop = Shop.find_by_slug(params[:slug])
	end

	def load_invoice( id )
                invoice =  @shop.invoices.find( id )
		paypal_request = Paypal::Express::Request.new( :username=> @shop.paypal_account.user,
							       :password=> @shop.paypal_account.password,
							       :signature=> @shop.paypal_account.signature )

		details = paypal_request.transaction_details( invoice.transaction_id )
		if details.ack.downcase != "success"
			return nil
		end

		subtotal = 0
		details.items.each do |item|
			item.description = Stuff.find( item.number ).description
			subtotal += item.amount.total
		end
		details.amount.item = subtotal

		transaction = Transaction.new( {:details=> details, :invoice_id=> id, :currency=> @shop.paypal_account.currency, :tracking=> invoice.tracking} )
	end

	def fulfill_order( invoice_id, tracking )
		invoice = Invoice.find( invoice_id )
		invoice.tracking = params[:tracking]
		invoice.processed = true
		invoice.ship_date = Time.now
		invoice.save
		transaction = load_invoice( invoice_id )
		OrderMailer.order_update( transaction, @shop ).deliver

		transaction.items.each do |item|
		  thing = Stuff.find(item[:id])
		  if thing.limited
		    thing.qty -= item[:quantity]
		    thing.save
		  end
		end
	end
end
