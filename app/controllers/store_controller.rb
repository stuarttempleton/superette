
class StoreController < ApplicationController
	before_filter :load_shop
	include ActionView::Helpers::NumberHelper

	def index
		@total = total
		render :template=> "store/index", :locals=>{:show_invoice=> 0}
	end

	def everything
		q = "(name like '%#{params[:search].gsub("'", "/'")}%' or description like '%#{params[:search].gsub("'", "/'")}%') and " if params[:search] != nil

		render :partial=>"stuff", :locals=> { :stuff=> Stuff.where(["#{q}shop_id = ? and id not in (select stuff_id from stacks) and not discontinued", @shop.id ]).order("id").page(params[:page])}
	end

	def discontinued
		render :partial=>"stuff", :locals=> { :stuff=> Stuff.where(["shop_id = ? and discontinued and id not in (select stuff_id from stacks) ", @shop.id ]).page(params[:page])}
	end

	def limited_run
		render :partial=>"stuff", :locals=> { :stuff=> Stuff.where(["shop_id = ? and not discontinued and limited and id not in (select stuff_id from stacks) ", @shop.id ]).page(params[:page])}
	end

	def chosen
		render :partial=>"stuff", :locals=> { :stuff=> Stuff.where("id in ( #{session[:basket].split(',').collect{|i| i.split(':')[0]}.sort.join(',')} )" ).page(params[:page])}
	end

	def stack
		render :partial=>"stuff", :locals=> { :stuff=> Stuff.find( params[:id], :conditions=>["shop_id = ? and not discontinued", @shop.id ]).stuffs.page(params[:page])}
	end

	def search
		query = '%' + params[:query].downcase + '%'
		render :partial=>"stuff", :locals=> { :stuff=>Stuff.where(["shop_id = ? and (lower(name) like ? or lower(description) like ?) and not discontinued", @shop.id, query, query]).page(params[:page]) }
	end

	def total
		tot = Money.new( 0, @shop.paypal_account.currency )
		aChosen = session[:basket].split(',')
		for i in aChosen
			item = Stuff.find(i.split(':')[0])
			if item.price
				tot += item.price * i.split(':')[1].to_i
			end
		end
		return tot
	end

	def quantity
		aChosen = session[:basket].split(',')
		if !aChosen.index{|x| x.split(':')[0] == params[:id]}
			aChosen << "#{params[:id]}:#{params[:qty]}" if params[:qty].to_i > 0
		else
			aChosen.collect!{|i|
				item = i.split(':')
				item[1] = item[1].to_i() + params[:qty].to_i if item[0] == params[:id]
				"#{item[0]}:#{item[1]}"
			}
		end

		session[:basket] = aChosen.join(',')

		#now remove whatever was reduced to 0 things.
		removed =  session[:basket].split(',').collect{|i| i.split(':')[0] if i.split(':')[1].to_i < 1}.compact
		session[:basket] = session[:basket].split(',').collect{|i| i if i.split(':')[1].to_i > 0}.compact.join(',')

		render :partial=>"quantity.js", :locals=>{:total=>total(), :removed=>removed, :id=>params[:id], :qty=>session[:basket].split(',').collect{|i| i.split(':')[1] if i.split(':')[0] == params[:id].to_s}.compact.join}

	end

	def choose
		s = Stuff.find(params[:id])
		script = ""
		aChosen = session[:basket].split(',')
		if s.available?

			if !aChosen.index{|x| x.split(':')[0] == params[:id]}
				aChosen << "#{params[:id]}:1"
				script = "$('#qty_#{params[:id]}').html('x1');$('#checkmark_#{params[:id]}').fadeIn('fast', function() {});"
				script += "$('div.hovernote').effect('bounce', { times:3 }, 300);" #replace
			else
				aChosen.delete_if{|x| x.split(':')[0] == params[:id]}
				script = "$('#qty_#{params[:id]}').html('x0');$('#checkmark_#{params[:id]}').fadeOut('fast', function() {});"  #replace
			end
			session[:basket] = aChosen.join(',')
		end

		script += "$('#total').html('#{number_to_currency(total(), :unit => @shop.paypal_account.currency_symbol)}');"
		script += "$('#load-stuff li.chosen').fadeOut('slow', function() {});" if aChosen.length < 1
		script += "$('#load-stuff li.chosen').fadeIn('slow', function() {});" if aChosen.length > 0
		render :text=>script
	end

	def dump
		#dump everything you've chosen
		session[:basket] = ""
		render :partial=>"store_streams"
	end

	def load_shop
		session[:basket] ||= ""
		@shop = Shop.find_by_slug(params[:slug])
		@total = Money.new( 0, @shop.paypal_account.currency )
	end

	def buy
		paypal_request, payment_request = create_paypal_requests()
		response = paypal_request.setup( payment_request, url_for( :controller=> 'store', :action=>'buy_success' ), url_for( :controller=> 'store', :action=>'buy_failure' ) )
		redirect_to response.redirect_uri
	end

	def buy_success
		# recreate our paypal request
		paypal_request, payment_request = create_paypal_requests()

		# complete the transaction here
		details = paypal_request.details( params[:token] )
		response = paypal_request.checkout!( params[:token], details.payer.identifier, payment_request )

		# if we get a bad ack here, we can exit early and just go back to the main page
		if response.ack.downcase != "success"
			@total = total
			flash[:notice] = "Paypal checkout failed"
			render :template=> "store/index", :locals=>{:show_invoice=> 0}
		end

		# if things are ok, save the invoice record
		invoice = Invoice.create( :transaction_id=>response.payment_info[0].transaction_id,
				       :email=>details.payer.email,
				       :zip=>details.ship_to.zip,
				       :shop_id=>@shop.id,
				       :processed=>false,
				       :canceled=>false,
				       :amount=>Money.new_with_dollars( details.amount.total, @shop.paypal_account.currency ) )
		# clean out the basket
		session[:basket] = ""

		# create a new transaction and render the invoice
		transaction = Transaction.new( {:details=> details, :invoice_id=> invoice.id, :currency=>@shop.paypal_account.currency} )
		render :template=> "store/index", :locals=>{:show_invoice=> 1, :transaction=> transaction}
	end

	def buy_failure
		@total = total
		flash[:notice] = "Paypal checkout canceled"
		render :template=> "store/index", :locals=>{:show_invoice=> 0}
	end

	private

	def create_paypal_requests
		paypal_request = Paypal::Express::Request.new( :username=> @shop.paypal_account.user,
							       :password=> @shop.paypal_account.password,
							       :signature=> @shop.paypal_account.signature)

		payment_request = Paypal::Payment::Request.new( :currency_code=> @shop.paypal_account.currency,
							        :shipping_amount=>shipping_total().to_f,
							        :amount=>total().to_f + shipping_total().to_f,
							        :description=>"" )
		payment_request.items = items()
		return paypal_request, payment_request
	end

	def items
		itemArray = Array.new
		purchasedItems = session[:basket].split(',')
		purchasedItems.each do |purchasedItem|
			thing = Stuff.find(purchasedItem.split(':')[0])
			itemArray << Paypal::Payment::Request::Item.new(:name=>thing.name,
									:description=>thing.description,
									:amount=>thing.price.to_f,
									:quantity=>purchasedItem.split(':')[1].to_f,
									:number=>thing.id)
		end
		return itemArray
	end

	def shipping_total
		ship_tot = Money.new( 0, @shop.paypal_account.currency )
		aChosen = session[:basket].split(',')
		for i in aChosen
			item = Stuff.find(i.split(':')[0])
			if item.shipping
				ship_tot += item.shipping * i.split(':')[1].to_i
			end
		end
		return ship_tot
	end

end
