class ShopsController < ApplicationController
	before_filter :check_auth

  # GET /shops
  # GET /shops.json
  def index
    @shops = Shop.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @shops }
    end
  end

  # GET /shops/1
  # GET /shops/1.json
  def show
    @shop = Shop.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @shop }
    end
  end

  # GET /shops/new
  # GET /shops/new.json
  def new
  	  @action = "create"
    @shop = Shop.new
    @shop.build_paypal_account

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @shop }
    end
  end

  # GET /shops/1/edit
  def edit

  	  @action = "update"
    @shop = Shop.find(params[:id])
  	  if !@edit
  	  	  redirect_to (@shop) and return
  	  end
	  @shop.build_paypal_account if not @shop.paypal_account
  end

  # POST /shops
  # POST /shops.json
  def create
    @shop = Shop.new(params[:shop])

    respond_to do |format|
      if @shop.save
        format.html { redirect_to "/#{@shop.slug}/shops/edit/#{@shop.id}", :notice => 'Shop was successfully created.' }
        format.json { render :json => @shop, :status => :created, :location => @shop }
      else
        format.html { render :action => "new" }
        format.json { render :json => @shop.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /shops/1
  # PUT /shops/1.json
  def update
    @shop = Shop.find(params[:id])
    #params[:shop][:slug] = params[:shop][:slug].strip.downcase.gsub(/[^a-z0-9]+/,'-')
    respond_to do |format|
      if @shop.update_attributes(params[:shop])
      	      notice = 'Shop was successfully updated.'
      	      if ( (params[:superpass] == params[:retype]) && (params[:retype] != ""))
      	      	      @shop.password = Shop.find_by_sql(["select password( ? ) as pass", params[:superpass]])[0].pass
      	      	      @shop.save
      	      	      notice += '<br />Shop password was updated!'
      	      end
        format.html { redirect_to "/#{@shop.slug}/shops/edit/#{@shop.id}", :notice => notice }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @shop.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /shops/1
  # DELETE /shops/1.json
  def destroy
    @shop = Shop.find(params[:id])
    @shop.destroy

    respond_to do |format|
      format.html { redirect_to shops_url }
      format.json { head :ok }
    end
  end

  def check_auth
  	  @edit = false
  	  @edit = true if session["authenticated"]
		
  end

end
