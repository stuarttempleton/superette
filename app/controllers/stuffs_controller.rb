class StuffsController < ApplicationController
	before_filter :check_auth

  # GET /stuffs
  # GET /stuffs.json
  def index
  	  @stuffs = @shop.stuffs.page(params[:page])


    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @stuffs }
    end
  end

  # GET /stuffs/1
  # GET /stuffs/1.json
  def show
    @stuff = Stuff.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @stuff }
    end
  end

  # GET /stuffs/new
  # GET /stuffs/new.json
  def new
  	  @action = "create"
 	  @stuff = Stuff.new({:shop_id=>@shop.id})

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @stuff }
    end
  end

  # GET /stuffs/1/edit
  def edit
  	  @action = "update"
    @stuff = Stuff.find(params[:id])
  	  if !@edit
  	  	  redirect_to (@stuff) and return
  	  end
  end

  # POST /stuffs
  # POST /stuffs.json
  def create
    @stuff = Stuff.new(params[:stuff])
    @action = "create"
    respond_to do |format|
      if @stuff.save
      	      format.html { redirect_to "/#{params[:slug]}/stuffs/edit/#{@stuff.id}", :notice => 'Stuff was successfully created.' }
        format.json { render :json => @stuff, :status => :created, :location => @stuff }
      else
        format.html { render :action => "new" }
        format.json { render :json => @stuff.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /stuffs/1
  # PUT /stuffs/1.json
  def update
    @stuff = Stuff.find(params[:id])
    @action = "update"
    respond_to do |format|
      if @stuff.update_attributes(params[:stuff])
      	      format.html { redirect_to "/#{@shop.slug}/stuffs/edit/#{@stuff.id}", :notice => 'Stuff was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @stuff.errors, :status => :unprocessable_entity }
      end
    end
  end


  def unstack
  	  s = Stack.find_by_container_id(params[:container_id], :conditions=>["stuff_id = ?",params[:stuff_id]])
  	  s.delete

  	  redirect_to "/#{params[:slug]}/stuffs/edit/#{params[:viewing]}", :notice => 'Item was unstacked.'
  end

  def stack
  	  Stack.new({:container_id=>params[:container_id], :stuff_id=>params[:stuff_id]}).save

  	  redirect_to "/#{params[:slug]}/stuffs/edit/#{params[:viewing]}", :notice => 'Item was stacked.'
  end

  def cloneandstack
  	  original = Stuff.find(params[:id])
  	  s = original.dup
  	  s.photo = original.photo
  	  s.save

  	  Stack.new({:container_id=>original.id, :stuff_id=>s.id}).save

  	  redirect_to "/#{@shop.slug}/stuffs/edit/#{s.id}", :notice => 'This is the fresh item! It has been stacked.'
  end

  def clone
  	  original = Stuff.find(params[:id])
  	  s = original.dup
  	  s.photo = original.photo
  	  s.save
  	  for stack in original.stacks
  	  	  newstack = stack.dup
  	  	  newstack.stuff_id = s.id
  	  	  newstack.save
  	  end

  	  redirect_to "/#{@shop.slug}/stuffs/edit/#{s.id}", :notice => 'This is the fresh item!'
  end


  # DELETE /stuffs/1
  # DELETE /stuffs/1.json
  def destroy
    @stuff = Stuff.find(params[:id])
    @stuff.destroy

    respond_to do |format|
      format.html { redirect_to "/#{@shop.slug}/stuffs/" }
      format.json { head :ok }
    end
  end

  def check_auth
  	  @edit = false
  	  @edit = true if session["authenticated"]
  	  @shop = Shop.find_by_slug(params[:slug])
  end

end
