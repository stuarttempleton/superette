class PaypalAccount < ActiveRecord::Base
  belongs_to :shop
  def currency_symbol
	  # determine the currency symbol based on configured paypal currency
	  return Money.new(100, currency).symbol
  end
end
