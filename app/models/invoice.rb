class Invoice < ActiveRecord::Base
        belongs_to :shop
  	composed_of :amount,
		:class_name => "Money",
		:mapping => [%w(amount_cents cents)],
		:constructor => Proc.new { |cents| Money.new(cents || 0, Money.default_currency) },
		:converter => Proc.new { |value| value.respond_to?(:to_money) ? value.to_money : raise(ArgumentError, "Can't convert #{value.class} to Money") }
	after_initialize :init_currency
	after_save :init_currency
	scope :new_orders, lambda{
		where( "invoices.processed = 0 AND invoices.canceled = 0" ).order( "invoices.created_at DESC" )
	}
	scope :processed_orders, lambda{
		where( "invoices.processed = 1").order( "invoices.ship_date DESC" )
	}
	scope :cancelled_orders, lambda{
		where( "invoices.canceled = 1").order( "invoices.created_at DESC" )
	}
	scope :next_new_order, lambda{ |current_id|
		new_orders.where( "invoices.id < ?", current_id ).limit( 1 )
	}
	def init_currency
                amount.currency_as_string = Shop.find(shop_id).paypal_account.currency if not amount.frozen?
	end
end
