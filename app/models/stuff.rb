class Stuff < ActiveRecord::Base
	paginates_per 16
	belongs_to :shop
	validates :name, :presence => true
	validates :description, :presence => true
	validates :qty, :presence => true
	after_initialize :init_currency
	after_save :init_currency

	has_attached_file :photo, :styles => {:viewable=>"250x250#",:detail=>"150x150#", :thumb=> "100x100#", :small  => "50x50>" }

	composed_of :price,
		:class_name => "Money",
		:mapping => [%w(price_cents cents)],
		:constructor => Proc.new { |cents| Money.new(cents || 0, Money.default_currency) },
		:converter => Proc.new { |value| value.respond_to?(:to_money) ? value.to_money : raise(ArgumentError, "Can't convert #{value.class} to Money") }

	composed_of :shipping,
		:class_name => "Money",
		:mapping => [%w(shipping_cents cents)],
		:constructor => Proc.new { |cents| Money.new(cents || 0, Money.default_currency) },
		:converter => Proc.new { |value| value.respond_to?(:to_money) ? value.to_money : raise(ArgumentError, "Can't convert #{value.class} to Money") }

	def init_currency
		# so this will overwrite the currency in the money object to what is
		# defined in shop.paypal_account.currency.
		price.currency_as_string = Shop.find(shop_id).paypal_account.currency if not price.frozen?
		shipping.currency_as_string = Shop.find(shop_id).paypal_account.currency if not shipping.frozen?
	end

	def stuffs
		Stuff.where(["id in ( ? )",Stack.find(:all, :conditions=>["container_id = ?", id]).collect{|x| x.stuff_id}.join(',')])
	end


	def discontinued_stuffs
		Stuff.find(Stack.find(:all, :conditions=>["container_id = ?", id]).collect{|x| x.stuff_id})
	end

	def stack?
		return true if stuffs.length > 0
		return false
	end

	def stacks
		Stack.find(:all, :conditions=>["stuff_id = ?", id])
	end

	def member_of
		Stuff.find(Stack.find(:all, :conditions=>["stuff_id = ?", id]).collect{|x| x.container_id})
	end

	def member?
		return true if member_of.length > 0
		return false
	end

	def available?
		return true if !limited
		return false if ( (qty == nil) && (limited == true) )
		return false if ( (qty < 1) && (limited == true) )
		return true
	end
end
