class Shop < ActiveRecord::Base
	validates :name, :uniqueness => true
	validates :email, :uniqueness => true
	validates :slug, :uniqueness => true
	
	validates :name, :presence => true
	validates :email, :presence => true
	validates :slug, :presence => true
	
	validates :password, :presence => true
	
	has_many :stuffs
	has_many :invoices
	has_one :paypal_account
	accepts_nested_attributes_for :paypal_account
	
	
	def self.authenticate(email = "", password = "")
		return false if password == ""
		return false if email == ""
		return true if find(:all, :conditions=>["email = ? and password = password(?)", email, password]).length > 0
	end
	
	def reset(request = nil)
		return false if request == nil
		return false if pendingpassword == nil
		
		self.password = pendingpassword if verification == request
		
		return verification == request
	end
	
	def initiatereset
		hash = rand(36**16).to_s(36)
		self.pendingpassword = Shop.find_by_sql(["select password( ? ) as pass", hash])[0].pass
		self.verification = hash
		
		#todo - send email
		
		return true
	end
	
end
