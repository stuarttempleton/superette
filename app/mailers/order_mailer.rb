
class OrderMailer < ActionMailer::Base
  add_template_helper(ApplicationHelper)

  def order_update( transaction, shop )
        @transaction = transaction
        @shop = shop
        mail(:to=> @transaction.email, :subject=> "Your #{@shop.name} order", :from=> @shop.email)
  end
end
