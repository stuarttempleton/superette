// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

$(document).ready(function() {
	$(function() {
              $('#load-stuff li.button').click(function(e) {
            		$.get( $(this).attr('href'), updateStuff );
            		e.preventDefault();
            	});
              $('nav a').click(function(e) {
            		$.get( $(this).attr('href'), updateStuff );
            		e.preventDefault();
            	});

	      // handle the search box
	      $("#search_input").keyup(function(e){
			if(e.keyCode == 13){
				$.get( $(this).attr('href') + "?" + $(this).serialize(), updateStuff );
			 }
		});

	      // handle invoice search stuff
	      $('#invoice_search_link').click( function() {
			$('#invoice_search_container').toggle('blind');
	      });

	      $('#invoice_search_form').keyup( function(e) {
			if(e.keyCode == 13){
				$('#status_text').html( 'Searching...' );
				$.get($('#invoice_search_form').attr('href') + "?" + $('#invoice_search_form').serialize(), invoiceResults );
			}
		});

	      // Invoice
	      $('#print_invoice').click( function(){
			$('#printable_area').printElement({
							   overrideElementCSS:['/assets/invoice.css'],
							   printMode:'popup'
							   });
	      });
	      $('#invoice_button_next').click( function(){
			$('#invoice_controls_form').attr( 'action', $('#invoice_button_next').attr('href') );
			$('#invoice_controls_form').submit();
	      });
	      $('#invoice_button_done').click( function(){
			$('#invoice_controls_form').attr( 'action', $('#invoice_button_done').attr('href') );
			$('#invoice_controls_form').submit();
	      });
	      $('#invoice_button_cancel').click( function(){
			$('#invoice_controls_form').attr( 'action', $('#invoice_button_cancel').attr('href') );
			$('#invoice_controls_form').submit();
	      });

        });

        if ( $('nav').length > 0 )
        	$.get( 'store/everything', updateStuff );

        registerStream();
});

function updateStuff( data ) {
	$('.stuff').quicksand( $(data).find('li'), {adjustHeight: 'dynamic'}, function(){registerStream();} );
	$('nav').html( $(data).find('nav').html());

	/*
		This makes sure we have next/prev links every refresh. this isn't yet used, though.
		I just don't want to redo this later.
	*/
	next = $(data).find('span.next a').attr('href');
	prev = $(data).find('span.prev a').attr('href');

	$('nav a').unbind();

	$('nav a').click(function(e) {
            		$.get( $(this).attr('href'), updateStuff );
            		e.preventDefault();
            	});

}

function invoiceResults( data ) {
	if ( data.indexOf( 'Error:' ) == 0 ){
		$('#status_text').html( data )
	} else {
		$('#status_text').html( 'Enter your email address and shipping zipcode to find previous orders' );
		$('#invoice_search_results').html( data );

		// We have to set up our invoice link listener here because
		// the invoice_link items don't exist at document.ready() time
		var junk = $('.invoice_link');
		$('.invoice_link').click( function(e) {
			  $('#status_text').html( 'Loading Invoice...' );
			  $.get($(e.target).attr('href'), showInvoice );
		});
	}
}

function showInvoice( data ){
	$('#status_text').html( 'Enter your email address and shipping zipcode to find previous orders' );

	if($('#store_content').is(":visible")){
		element = $('#store_content');
	} else {
		element = $('#invoice_content');
	}

	element.fadeOut( function(){
		$('#invoice_content').html( data );
		$('#invoice_content').fadeIn();
		$('#print_invoice').click( function(){
			$('#printable_area').printElement({
							   overrideElementCSS:['/assets/invoice.css'],
							   printMode:'popup'
							   });
		});
		$('#return_to_store').click( function(){
			$('#invoice_content').fadeOut( function(){$('#store_content').fadeIn()});
		});

	});
}

function registerStream(){
	$(function() {
		$('div.stack_link').unbind();
		$('div.stack_link').click(function(e) {
			$.get( $(this).attr('href'), function(data) {
					$('.stuff').quicksand( $(data).find('li'), {adjustHeight: 'dynamic'}, function(){registerStream();});
					$('nav').html( $(data).find('nav').html() );
            		});
            		e.preventDefault();
		});

		$('img.arrow').unbind();
		$('img.arrow').click(function(e) {
			$.get( $(this).attr('href'), function(data) {
					eval(data);
            		});
            		e.preventDefault();
            		e.stopPropagation();
		});


        });

        initPopups();
}

function initPopups(){
	$(function () {
	  $('.has_popup').each(function () {
	    // options
	    var distance = 10;
	    var time = 250;
	    var hideDelay = 50;

	    var hideDelayTimer = null;

	    // tracker
	    var beingShown = false;
	    var shown = false;

	    var trigger = $('.trigger', this);
	    var popup = $('.popup', this).css('opacity', 0);

	    // set the mouseover and mouseout on both element
	    $([trigger.get(0), popup.get(0)]).mouseover(function () {
	      // stops the hide event if we move from the trigger to the popup element
	      if (hideDelayTimer) clearTimeout(hideDelayTimer);

	      // don't trigger the animation again if we're being shown, or already visible
	      if (beingShown || shown) {
		return;
	      } else {
		beingShown = true;

		// reset position of popup box
		popup.css({
		  top: -75,
		  left: -80,
		  display: 'block' // brings the popup back in to view
		})

		// (we're using chaining on the popup) now animate it's opacity and position
		.animate({
		  top: '-=' + distance + 'px',
		  opacity: 1
		}, time, 'swing', function() {
		  // once the animation is complete, set the tracker variables
		  beingShown = false;
		  shown = true;
		});
	      }
	    }).mouseout(function () {
	      // reset the timer if we get fired again - avoids double animations
	      if (hideDelayTimer) clearTimeout(hideDelayTimer);

	      // store the timer so that it can be cleared in the mouseover if required
	      hideDelayTimer = setTimeout(function () {
		hideDelayTimer = null;
		popup.animate({
		  top: '-=' + distance + 'px',
		  opacity: 0
		}, time, 'swing', function () {
		  // once the animate is complete, set the tracker variables
		  shown = false;
		  // hide the popup entirely after the effect (opacity alone doesn't do the job)
		  popup.css('display', 'none');
		});
	      }, hideDelay);
	    });
	  });
	});
}
