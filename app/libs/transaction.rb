class Transaction

	# A ton of attribute accessors
	# Customer info
	attr_accessor :name, :email, :street, :city, :state, :zip

	# invoice stuff
	attr_accessor :items, :invoice_id, :subtotal, :tax, :shipping, :grandtotal, :tracking
	def initialize( args )
		# take details and response from the paypal results
		if args.length > 2

			# populate customer info
			currency = args[:currency]
			@name = args[:details].ship_to.name
			@email = args[:details].payer.email
			@street = args[:details].ship_to.street
			@city = args[:details].ship_to.city
			@state = args[:details].ship_to.state
			@zip = args[:details].ship_to.zip

			# populate the rest of the transaction info
			@invoice_id = args[:invoice_id]
			@subtotal = Money.new_with_dollars( args[:details].amount.item, currency)
			@tax = Money.new_with_dollars( args[:details].amount.tax, currency )
			@shipping = Money.new_with_dollars( args[:details].amount.shipping, currency )
			@grandtotal = Money.new_with_dollars( args[:details].amount.total, currency )
			@tracking = args[:tracking]

			# now process the items
			response_items = args[:details].items ? args[:details].items : args[:details].payment_responses[0].items
			@items = Array.new
			response_items.each do |item|
				price = Money.new_with_dollars( item.amount.total, currency )
				@items << { :quantity=> item.quantity, :name=> item.name, :description=> item.description,
					:item_price=>price, :total=>( price * item.quantity ), :id=>item.number }
			end
		end
	end
end
