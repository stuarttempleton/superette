# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120306214631) do

  create_table "invoices", :force => true do |t|
    t.string   "transaction_id"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "shop_id"
    t.string   "tracking"
    t.string   "zip"
    t.boolean  "processed"
    t.integer  "amount_cents"
    t.date     "ship_date"
    t.boolean  "canceled"
  end

  create_table "paypal_accounts", :force => true do |t|
    t.string   "user"
    t.string   "password"
    t.string   "signature"
    t.integer  "shop_id"
    t.string   "currency"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "paypals", :force => true do |t|
    t.string   "user"
    t.string   "password"
    t.string   "signature"
    t.integer  "shop_id"
    t.string   "currency"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shops", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "slug"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "verification"
    t.string   "pendingpassword"
    t.string   "description"
  end

  create_table "stacks", :force => true do |t|
    t.integer  "container_id"
    t.integer  "stuff_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stuffs", :force => true do |t|
    t.text     "name"
    t.text     "description"
    t.integer  "qty"
    t.boolean  "discontinued"
    t.integer  "shop_id"
    t.datetime "expires_at"
    t.boolean  "limited"
    t.text     "photo_file_name"
    t.text     "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "price_cents"
    t.integer  "shipping_cents"
  end

end
