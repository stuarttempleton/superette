class AddProcessedToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :processed, :boolean
  end
end
