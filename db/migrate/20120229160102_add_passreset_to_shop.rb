class AddPassresetToShop < ActiveRecord::Migration
  def change
    add_column :shops, :verification, :string
    add_column :shops, :pendingpassword, :string
  end
end
