class AddPaypalIdToShop < ActiveRecord::Migration
  def change
    add_column :shops, :paypal_id, :integer
  end
end
