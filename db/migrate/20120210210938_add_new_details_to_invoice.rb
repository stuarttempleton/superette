class AddNewDetailsToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :shop_id, :integer
    add_column :invoices, :tracking, :string
    add_column :invoices, :zip, :string
  end
end
