class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :transaction_id
      t.string :email

      t.timestamps
    end
  end
end
