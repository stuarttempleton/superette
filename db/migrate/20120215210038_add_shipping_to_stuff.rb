class AddShippingToStuff < ActiveRecord::Migration
  def change
    add_column :stuffs, :shipping_cents, :integer
  end
end
