class AddPriceToStuff < ActiveRecord::Migration
  def change
    add_column :stuffs, :price, :decimal
  end
end
