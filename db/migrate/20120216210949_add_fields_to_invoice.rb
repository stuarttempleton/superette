class AddFieldsToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :ship_date, :date
    add_column :invoices, :canceled, :boolean
  end
end
