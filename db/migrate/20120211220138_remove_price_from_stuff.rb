class RemovePriceFromStuff < ActiveRecord::Migration
  def up
    remove_column :stuffs, :price
  end

  def down
    add_column :stuffs, :price, :decimal
  end
end
