class AddDescriptionToShops < ActiveRecord::Migration
  def change
    add_column :shops, :description, :string
  end
end
