class AddPriceCentsToStuff < ActiveRecord::Migration
  def change
    add_column :stuffs, :price_cents, :integer
  end
end
