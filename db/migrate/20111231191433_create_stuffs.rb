class CreateStuffs < ActiveRecord::Migration
  def change
    create_table :stuffs do |t|
      t.text :name
      t.text :description
      t.integer :qty
      t.boolean :discontinued
      t.integer :shop_id
      t.datetime :expires_at
      t.boolean :limited
      
      t.text :photo_file_name
      t.text :photo_content_type
      t.integer :photo_file_size

      t.timestamps
    end
  end
end
