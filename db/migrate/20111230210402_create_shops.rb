class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.string :email
      t.string :slug
      t.string :password

      t.timestamps
    end
  end
end
