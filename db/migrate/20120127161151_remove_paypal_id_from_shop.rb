class RemovePaypalIdFromShop < ActiveRecord::Migration
  def up
    remove_column :shops, :paypal_id
  end

  def down
    add_column :shops, :paypal_id, :integer
  end
end
