class CreateStacks < ActiveRecord::Migration
  def change
    create_table :stacks do |t|
      t.integer :container_id
      t.integer :stuff_id

      t.timestamps
    end
  end
end
