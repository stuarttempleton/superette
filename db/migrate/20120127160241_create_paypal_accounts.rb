class CreatePaypalAccounts < ActiveRecord::Migration
  def change
    create_table :paypal_accounts do |t|
      t.string :user
      t.string :password
      t.string :signature
      t.integer :shop_id
      t.string :currency

      t.timestamps
    end
  end
end
