class AddAmountToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :amount_cents, :integer
  end
end
