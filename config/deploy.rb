# This is used to make sure the custom databse.yml file gets migrated with each deployment
# There may be a better way to do this, but here is the site I referenced: http://www.simonecarletti.com/blog/2009/06/capistrano-and-database-yml/
require "./config/capistrano_database"

set :application, "superette.openformfoundation.org"
set :repository,  "git://github.com/stuarttempleton/Superette.git"
set(:user) { Capistrano::CLI.ui.ask("User name: ") }
set :appuser, "superetteu"

set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
set :scm_username, user
set :runner, user
set :use_sudo, false
set :branch, "master"
set :deploy_via, :checkout
set :git_shallow_clone, 1
set :deploy_to, "/home/#{appuser}/#{application}"


default_run_options[:pty] = true
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "id_dsa")]
role :web, "www.openformfoundation.org"                          # Your HTTP server, Apache/etc
role :app, "www.openformfoundation.org"                          # This may be the same as your `Web` server
role :db,  "www.openformfoundation.org", :primary => true # This is where Rails migrations will run

# global rvm integration
#$:.unshift(File.expand_path('./lib', ENV['rvm_path'])) # Add RVM's lib directory to the load path.
require "rvm/capistrano"                  # Load RVM's capistrano plugin.
set :rvm_ruby_string, '1.9.2-p290@rails313'
set :rvm_type, :system

require "bundler/capistrano"

load 'deploy/assets'

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
end

after "deploy", "deploy:migrate"
