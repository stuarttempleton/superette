require 'money'

# paypal plugin
require 'paypal'

# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Superette::Application.initialize!
